<?php

namespace App\Http\Controllers\Room;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CateRepository;
use App\Repositories\RoomRepository;
use App\Repositories\DistrictRepository;
use App\Repositories\UserRepository;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreChangePassUserPost;
class RoomController extends Controller
{
    private $repository;
    private $stt =1;
    function __construct(RoomRepository $room)
    {
        $this->repository = $room;
    }
    //hàm xử lý trang index
    public function index()
    {
//        $cates = $cate->getDataIndex();
//        $rooms_max_view = $this->repository->getRoomByCountView();
//        $data['cates'] = $cates;
//        $data['rooms_max_view'] = $rooms_max_view;
        $rooms = $this->repository->getDataIndex();
        $data['rooms'] = $rooms;
//        return view('motel_room.index',$data);
        return view('rooms.index',$data);
    }
    //hàm xử lí login
    public function login()
    {
        return view('rooms.login');
    }
    // page đăng bài
    public function post(CateRepository $cate,DistrictRepository $district)
    {
        $districts = $district->getDataIndex();
        $cates = $cate->getDataIndex();
        $data['cates'] = $cates;
        $data['districts'] = $districts;
        return view('rooms.post',$data);
    }
    public function save($id,Request $request)
    {
        $title = $request->title;
        $price = $request->price;
        $acreage = $request->acreage;
        $district_id = $request->district_id;
        $commune_id  = $request->commune_id;
        $address = $request->address;
        $cate_id = $request->cate_id;
        $phone = $request->phone;
        $description = html_entity_decode($request->description);
        $files  = $request->file('images');
        if($files)
        {
            foreach ($files as $file)
            {
                $nameFile = $file->getClientOriginalName();
                if($nameFile)
                {
                    $namefileArray[] = $nameFile;
                    $file->move('rooms/images/rooms',$file->getClientOriginalName());
                }
            }
        }
        else{
            $namefileArray[] = null;
        }
        $fileJson = json_encode($namefileArray);
        $data = [
            'title'        => $title,
            'price'        => $price,
            'acreage'      => $acreage,
            'district_id'  => $district_id,
            'commune_id'   => $commune_id,
            'address'      => $address,
            'cate_id'      => $cate_id,
            'phone'        => $phone,
            'description'  => $description,
            'user_id'      =>\Auth::guard('web')->user()->id,
            'images'       => $fileJson,
        ];
        return $this->repository->updateOrCreate($id,$data);
    }
    public function handlePost($id=null,Request $request)
    {
        $data = $this->save($id,$request);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã đăng tin thành công. Vui lòng chờ quản trị viên duyệt bài đăng!');
            return redirect()->route('room.post');
        }
        $request->session()->flash('thongbao', 'Bạn đã đăng tin thất bại. Xin vui lòng thử lại!');
        return redirect()->route('room.post');
    }
    // hàm xử lý đăng ký
    public function create()
    {
        return view('rooms.create');
    }
    public function destroy($id,Request $request)
    {
        $data = $this->repository->destroy($id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã xoá bài viết thành công!');
            return redirect()->route('room.user.info');
        }
        $request->session()->flash('thongbao', 'Bạn đã xóa bài viết thất bại!');
        return redirect()->route('room.user.info');
    }
    public function search(Request $request)
    {
        if($request->ajax())
        {
            $district_id = (int)$request->district_id;
            $commune_id  = (int)$request->commune_id;
            $cate_id     = (int)$request->cate_id;
            $minprice = (int)$request->minprice;
            $maxprice = (int)$request->maxprice;
            $data = $this->repository->search($district_id,$commune_id,$cate_id,$minprice,$maxprice);
            $string = '';

//            path images
//            '.URL::to('/rooms/images/rooms').'/'.json_decode($value->images)[0].'
            if($data){
                foreach ($data as $value)
                {

                    $string .= '<div class="col-xs-6 col-md-3 animation masonry-item">
                                        <div class="pgl-property">
                                            <div class="property-thumb-info">
                                                <div class="property-thumb-info-image">
                                                    <img alt="" class="img" src="'.asset('rooms/images/rooms').'/'.json_decode($value->images)[0].'" height="150px" width="100%">
                                                    <span class="property-thumb-info-label">
    													<span class="label price">'.number_format($value->price).'</span>
    													<span class="label forrent">Rent</span>
    												</span>
                                                </div>
                                                <div class="property-thumb-info-content">
                                                    <h3><a href=""><b>'.$value->title.'</b></a></h3>
                                                    <address><b>Địa điểm: '.$value->address.'</b></address>
                                                    <p><b>Thời gian đăng: </b>'.$value->created_at.'</p>
                                                </div>
                                                <div class="amenities clearfix">
                                                    <ul class="pull-left">
                                                        <li><strong>Diện tích:</strong> '.$value->acreage.'<sup>m2</sup></li>
                                                    </ul>
                                                    <ul class="pull-right">
                                                        <li><i class="fa fa-eye"></i> Lượt xem: '.$value->count_views.'</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>';
                }
            }
            else{
                $request->session()->flash('thongbao','Không tìm thấy kết quả nào!');
                $string = '<b>Không tìm thấy kết quả nào !</b>';
            }
            return $string;
        }
    }
    public function listFullWidth()
    {
        $rooms = $this->repository->getDataIndex();
        $data['rooms'] = $rooms;
        return view('rooms.listFullWidth',$data);
    }

    //hàm xử lý thông tin người dùng
    public function informationUser(UserRepository $user,Request $request)
    {
        if($request->ajax())
        {
            $postByUser = \Auth::guard('web')->user()->room()->orderBy('created_at','DESC')->get();
            return Datatables::of($postByUser)
                ->addColumn('stt',function ($data){
                    return $this->stt++;
                })
                ->editColumn('cate_id',function ($data){
                    if($data->cate_id) return $data->category->name;
                })
                ->editColumn('approve',function ($data){
                    if($data->approve == 0) return '<a href="" class="btn btn-xs btn-drank"><i class="fa fa-clock-o"></i> Đang chờ phê duyệt</a>';
                    if($data->approve == 1) return '<a href="" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Đã phê duyệt</a>';
                    if($data->approve == 2) return '<a href="" class="btn btn-xs btn-danger"><i class="fa fa-key"></i> Đã khóa tin</a>';
                })
                ->addColumn('action',function ($data){
                    $string = '
                      <a href="" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Sửa</a>
                      <a href="" data-href="" class="btn btn-xs btn-danger btn-delete"  data-toggle="modal" data-target="#modalDelete"><i class="fa fa-times"></i> Xóa</a>
                      
                      <!-- Modal Xóa -->
                        <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
                                    </div>
                                    <div class="modal-body">
                                        Bạn có chắc chắn muốn xóa ?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                                        <a href="'.route('room.post.destroy',$data->id).'" class="btn btn-primary">Xóa</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                      ';
                    if($data->status == 0 && $data->approve == 1)
                    {
                        $string .= '<a href="'.route('room.post.key',['id_post'=>$data->id , 'status'=>1]).'" class="btn btn-xs btn-warning"><i class="fa fa-key"></i> Khóa tin</a>';
                    }
                    if($data->status == 1 && $data->approve == 2)
                    {
                        $string .= '<a href="'.route('room.post.key',['id_post'=>$data->id , 'status'=>0]).'" class="btn btn-xs"><i class="fa fa-key"></i> Mở tin</a>';
                    }
                    return $string;
                })
                ->rawColumns(['stt','cate_id','approve','action'])
                ->toJson();
        }
        return view('rooms.user.info');
    }
    public function detailsRoom($id)
    {
        $room = $this->repository->getDataById($id);
        $comments = $room->comment()->orderBy('created_at','DESC')->paginate(6);
        $room->count_views++;
        $room->save();
        $roomsRandom = $this->repository->getRoomRandom();
        $data['room'] = $room;
        $data['roomsRandom'] = $roomsRandom;
        $data['comments'] = $comments;
        return view('rooms.detailsRoom',$data);
    }
    public function key($id_post,$status,Request $request)
    {
        $room = $this->repository->getDataById($id_post);
        $room->status = $status;
        if($status == 1)
        {
            $room->approve = 2;
            $room->save();
            if($room)
            {
                $request->session()->flash('thongbao', 'Bạn đã khóa bài viết thành công!');
                return redirect()->route('room.user.info');
            }
        }
        if($status == 0)
        {
            $room->approve = 1;
            $room->save();
            if($room)
            {
                $request->session()->flash('thongbao', 'Bạn đã mở bài viết thành công!');
                return redirect()->route('room.user.info');
            }
        }

    }

    // Controller User
    public function saveUser($id,Request $request,UserRepository $user)
    {

        $userOne = $user->getDataById($id);
        $name = $request->name;
        $username = $request->username;
        $email = $request->email;
        $status = $request->status;
        $avatar = $userOne->avatar;
        if($file = $request->file('avatar'))
        {
            $nameFile = $file->getClientOriginalName();
            if($nameFile)
            {
                $avatar = $nameFile;
                $file->move('rooms/images/user',$nameFile);
            }
        }
        $data = [
          'name' => $name,
          'username' => $username,
          'avatar' => $avatar,
          'email' => $email,
          'status' => $status,
        ];
        $userOne->avatar = $avatar;
        $userOne->save();
//        dd($avatar);
        return $user->updateOrCreate($data,$id);
    }
    public function destroyUser($id,UserRepository $user,Request $request)
    {
        $data = $user->destroy($id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã xoá tài khoản thành công!');
            return redirect()->route('room.login');
        }
        $request->session()->flash('thongbao', 'Bạn đã xóa tài khoản thất bại!');
        return redirect()->route('room.login');
    }
    public function updateUser($id,Request $request,UserRepository $user){
        $data = $this->saveUser($id,$request,$user);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã sửa thông tin trang cá nhân thành công!');
            return redirect()->route('room.user.info');
        }
        $request->session()->flash('thongbao', 'Bạn đã sửa thông tin trang cá nhân thất bại!');
        return redirect()->route('room.user.info');
    }
    public function checkPassword()
    {
        return view('rooms.user.checkPass');
    }
    public function handleChangePassword(StoreChangePassUserPost $request,UserRepository $user)
    {
//        $newPass = $request->newPass;
    }
    public function getRoomByCate(Request $request,$cateId)
    {
        if($request->ajax())
        {
            $order = $request->order;
            $sort = $request->sort;
            $data = $this->repository->getRoomByCateAndFilter($cateId,$sort,$order);
            $string  = '';
            foreach ($data as $value)
            {
                $string  .= '<div class="col-xs-6 col-md-3 animation masonry-item">
                                        <div class="pgl-property">
                                            <div class="property-thumb-info">
                                                <div class="property-thumb-info-image">
                                                   <img alt="" class="" src="'.asset('rooms/images/rooms').'/'. json_decode($value->images)[0].'" height="150px" width="100%">
                                                    <span class="property-thumb-info-label">
    													<span class="label price">'.number_format($value->price).'</span>
    													<span class="label forrent">Rent</span>
    												</span>
                                                </div>
                                                <div class="property-thumb-info-content">
                                                    <h3><a href="'.route("room.details",$value->id).'"><b>'.$value->title.'</b></a></h3>
                                                    <address><b>Địa điểm:</b>'.$value->address.'</address>
                                                    <p><b>Thời gian đăng:</b>'.$value->created_at.'</p>
                                                </div>
                                                <div class="amenities clearfix">
                                                    <ul class="pull-left">
                                                        <li><strong>Diện tích:</strong>'.$value->acreage.'<sup>m2</sup></li>
                                                    </ul>
                                                    <ul class="pull-right">
                                                        <li><i class="fa fa-eye"></i> Lượt xem: '.$value->count_views.'</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>';
            }
            return $string;
        }
        $rooms = $this->repository->getRoomByCate($cateId);
        $data['rooms']=$rooms;
        $data['cateId']=$cateId;
        return view('rooms.room.roomByCate',$data);
    }
    public function getRoomByCateFullWidth($cateId)
    {
        $rooms = $this->repository->getRoomByCate($cateId);
        $data['rooms']=$rooms;
        $data['cateId']=$cateId;
        return view('rooms.room.roomByCateFullWidth',$data);
    }
    public function filterRoom(Request $request)
    {
        if($request->ajax())
        {
            $order = $request->order;
            $sort = $request->sort;
            $data = $this->repository->getRoomByFilter($sort,$order);
            $string  = '';
            foreach ($data as $value)
            {
                $string  .= '<div class="col-xs-6 col-md-3 animation masonry-item">
                                        <div class="pgl-property">
                                            <div class="property-thumb-info">
                                                <div class="property-thumb-info-image">
                                                   <img alt="" class="" src="'.asset('rooms/images/rooms').'/'. json_decode($value->images)[0].'" height="150px" width="100%">
                                                    <span class="property-thumb-info-label">
    													<span class="label price">'.number_format($value->price).'</span>
    													<span class="label forrent">Rent</span>
    												</span>
                                                </div>
                                                <div class="property-thumb-info-content">
                                                    <h3><a href="'.route("room.details",$value->id).'"><b>'.$value->title.'</b></a></h3>
                                                    <address><b>Địa điểm:</b>'.$value->address.'</address>
                                                    <p><b>Thời gian đăng:</b>'.$value->created_at.'</p>
                                                </div>
                                                <div class="amenities clearfix">
                                                    <ul class="pull-left">
                                                        <li><strong>Diện tích:</strong>'.$value->acreage.'<sup>m2</sup></li>
                                                    </ul>
                                                    <ul class="pull-right">
                                                        <li><i class="fa fa-eye"></i> Lượt xem: '.$value->count_views.'</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>';
            }
            return $string;
        }
    }
    public function checkPass(Request $request,UserRepository $user)
    {
        $oldPass = $request->oldPass;
        $userOne = $user->getDataById(\Auth::guard('web')->user()->id);
        if(Hash::check($oldPass,$userOne->password))
        {
            return view('rooms.user.changePass');
        }
        return redirect()->route('room.user.checkPassword');
    }
}
