<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\AdminRepository;
use Yajra\Datatables\Datatables;
use App\Http\Requests\StoreAddAdminPost;
use App\Http\Requests\StoreEditAdminPost;
class AdminController extends Controller
{
    private $repository;
    function __construct(AdminRepository $admin)
    {
        $this->repository = $admin;

    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->editColumn('role',function ($data){
                    if($data->role == 0) return '<b style="color: red">Admin</b>';
                })
                ->editColumn('status',function ($data){
                    if($data->status == 1) return '<i style="color: green">Hoạt động</i>';
                    return '<i style="color: red">Tạm khóa</i>';
                })
                ->addColumn('action',function ($data){
                    $string = '
                      <a href="'.route('admin.admin.edit',$data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                      <a href="'.route('admin.admin.destroy',$data->id).'" data-href="" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                      ';
                    return $string;
                })
                ->rawColumns(['action','role','status'])
                ->toJson();
        }
        return view('admin.admin.index');
    }
    public function create()
    {
        return view('admin.admin.create');
    }
    public function save(Request $request,$id)
    {
        $username = $request->username;
        $email    = $request->email;
        $fullname = $request->hoten;
        $password = $id ? $request->password :bcrypt($request->password);
        $role     = $request->role;
        $status   = $request->status;
        $data = [
            'username' => $username,
            'email'    => $email,
            'name' => $fullname,
            'password' => $password,
            'role'     => $role,
            'status'   => $status
        ];
        return $this->repository->updateOrCreate($data,$id);
    }
    public function store($id=null,StoreAddAdminPost $request)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã tạo tài khoản thành công!');
            return redirect()->route('admin.admin.index');
        }
        $request->session()->flash('thongbao', 'Bạn đã tạo tài khoản thất bại!');
        return redirect()->route('admin.admin.index');

    }
    public function edit($id)
    {
        $admin = $this->repository->getDataById($id);
        return view('admin.admin.edit',['admin'=>$admin]);
    }
    public function update($id,StoreEditAdminPost $request)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã sửa tài khoản thành công!');
            return redirect()->route('admin.admin.index');

        }
        $request->session()->flash('thongbao', 'Bạn đã sửa tài khoản thất bại!');
        return redirect()->route('admin.admin.index');

    }
    public function destroy($id,Request $request)
    {
        $data = $this->repository->destroy($id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã xoá tài khoản thành công!');
            return redirect()->route('admin.admin.index');
        }
        $request->session()->flash('thongbao', 'Bạn đã xóa tài khoản thất bại!');
        return redirect()->route('admin.admin.index');
    }

}
