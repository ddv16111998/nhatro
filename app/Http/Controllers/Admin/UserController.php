<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Repositories\UserRepository;
use App\Http\Requests\StoreEditUserPost;
use App\Http\Requests\StoreAddUserPost;

class UserController extends Controller
{
    private $repository;

    function __construct(UserRepository $user)
    {
        $this->repository = $user;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->editColumn('role', function ($data) {
                    if ($data->role == 1) return '<b style="color: red">Người dùng</b>';
                })
                ->editColumn('status', function ($data) {
                    if ($data->status == 1) return '<i style="color: green">Hoạt động</i>';
                    return '<i style="color: red">Tạm khóa</i>';
                })
                ->addColumn('action', function ($data) {
                    $string = '
                      <a href="' . route('admin.user.edit', $data->id) . '" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                      <a href="' . route('admin.user.destroy', $data->id) . '" data-href="" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                      ';
                    return $string;
                })
                ->rawColumns(['action', 'role', 'status'])
                ->toJson();
        }
        return view('admin.user.index');
    }
    public function save(Request $request,$id)
    {
        $username = $request->username;
        $email    = $request->email;
        $fullname = $request->hoten;
        $password = $id ? $request->password :bcrypt($request->password);
        $role     = 1;
        $status   = $id ? $request->status : 1;
        $data = [
            'username' => $username,
            'email'    => $email,
            'name'     => $fullname,
            'password' => $password,
            'role'     => $role,
            'status'   => $status
        ];
        return $this->repository->updateOrCreate($data,$id);
    }
    public function store($id=null,StoreAddUserPost $request)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã tạo tài khoản người dùng thành công!');
            return redirect()->route('room.login');

        }
        $request->session()->flash('thongbao', 'Bạn đã tạo tài khoản người dùng thất bại!');
        return redirect()->route('room.login');
    }
    public function edit($id)
    {
        $user = $this->repository->getDataById($id);
        return view('admin.user.edit',['user'=>$user]);
    }
    public function update($id,StoreEditUserPost $request)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã sửa tài khoản người dùng thành công!');
            return redirect()->route('admin.user.index');

        }
        $request->session()->flash('thongbao', 'Bạn đã sửa tài khoản người dùng thất bại!');
        return redirect()->route('admin.user.index');

    }
    public function destroy($id,Request $request)
    {
        $data = $this->repository->destroy($id);
        if($data)
        {
            $request->session()->flash('thongbao', 'Bạn đã xoá tài khoản thành công!');
            return redirect()->route('admin.user.index');
        }
        $request->session()->flash('thongbao', 'Bạn đã xóa tài khoản thất bại!');
        return redirect()->route('admin.user.index');
    }
}
