<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\StoreLoginPost;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = "/";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function guard()
    {
        return Auth::guard('admins');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }
    public function sendFailedLoginResponse(Request $request)
    {
        $user = Admin::where(['email'=>$request->email])->first();
        if($user && \Hash::check($request->password,$user->password) && $user->status !=1)
        {
            $request->session()->flash('thongbao','Tài khoản đã bị vô hiệu hóa!');
            return redirect()->route('login');
        }
    }
    protected function login(StoreLoginPost $request)
    {
        if($this->sendFailedLoginResponse($request))
        {
            return $this->sendFailedLoginResponse($request);
        }
        $data = $this->guard()->attempt(['email'=>$request->email, 'password'=>$request->password, 'status'=>1,'role'=>0],$request->remember);
        if($data)
        {
            return redirect()->route('admin.dashboard.index');
        }
        else{
            $request->session()->flash('thongbao','Thông tin tài khoản / mật khẩu không chính xác!');
            return redirect()->route('login');
        }
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        return redirect()->route('login');
    }


}
