<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEditAdminPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hoten'=> 'required',
            'password_verified'=>'same:password'
        ];
    }
    public function messages()
    {
        return [
            'hoten.required'=> 'Họ tên không được để trống!',
            'password_verified.same'   => 'Mật khẩu chưa trùng khớp!',
        ];
    }
}
