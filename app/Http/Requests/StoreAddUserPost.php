<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddUserPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=> 'required|unique:users',
            'email'   => 'required|unique:users',
            'hoten'   => 'required',
            'password'=> 'min:8|max:32',
            'password_verified'=>'same:password',
        ];
    }
    public function messages()
    {
        return [
            'username.required' => 'Tài khoản không được để trống!',
            'username.unique'   => 'Tài khoản đã tồn tại!',
            'email.required'    => 'Email không được để trống!',
            'email.unique'      => 'Email khoản đã tồn tại!',
            'hoten.required'    => ' Họ tên không được để trống',
            'password.min'      => 'Mật khẩu chứa ít nhất :min kí tự',
            'password.max'      => 'Mật khẩu chứa nhiều nhất :max kí tự',
            'password_verified.same' => 'Mật khẩu chưa trùng khớp!',
        ];
    }
}
