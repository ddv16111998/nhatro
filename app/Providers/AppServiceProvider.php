<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\Repositories\DistrictRepository;
use App\Repositories\CateRepository;
use App\Repositories\UserRepository;
use App\Repositories\RoomRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    
    public function boot(DistrictRepository $district,CateRepository $cate,UserRepository $user,RoomRepository $room)
    {
        Schema::defaultStringLength(191);
        header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0');
        $data['districts'] = $district->getDataIndex();
        $data['cates']     = $cate->getDataIndex();
        $data['countApproved'] = $room->getCountRoomNeedApproved();
        View::share($data);
    }
}
