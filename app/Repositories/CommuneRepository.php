<?php
namespace App\Repositories;
use App\Models\Commune;
class CommuneRepository
{
    public function getDataIndex()
    {
        $data = Commune::whereNull('deleted_at')->get();
        return $data;
    }
    public function getDataByDistrict($district_id)
    {
        $data = Commune::where('district_id',$district_id)->get();
        return $data;
    }

}
