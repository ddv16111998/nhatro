<?php
namespace App\Repositories;
use App\Models\Comments;
class CommentRepository
{
    public function getDataIndex()
    {

    }
    public function getDataByPost()
    {

    }
    public function updateOrCreate($id,$data)
    {
        $data = Comments::updateOrCreate(['id'=>$id],$data);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function getDataLast()
    {
        $data = Comments::all()->last();
        return $data;
    }
}
