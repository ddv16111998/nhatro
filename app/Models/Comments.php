<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comments extends Model
{
    use SoftDeletes;
    protected $table ='comments';
    protected $guarded=[];
    protected $dates =['deleted_at'];
    public function post()
    {
        return $this->belongsTo('App\Models\Room','post_id');
    }
    public function userComment()
    {
        return $this->belongsTo('App\Models\User','user_comment_id');
    }
}
