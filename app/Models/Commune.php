<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Commune extends Model
{
    use SoftDeletes;
    protected $table = 'communes';
    protected $guarded = [];
    protected $dates =['deleted_at'];

    public function district()
    {
        return $this->belongsTo('App\Models\District','district_id');
    }
}
