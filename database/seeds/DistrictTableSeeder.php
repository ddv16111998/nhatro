<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('district')->insert([
            ['name'=>'Mỹ Đức','slug'=>'my-duc'],
            ['name'=>'Ba Đình','slug'=>'ba-dinh'],
            ['name'=>'Hoàn Kiếm','slug'=>'hoan-kiem'],
            ['name'=>'Tây Hồ','slug'=>'tay-ho'],
            ['name'=>'Long Biên','slug'=>'long-bien'],
            ['name'=>'Cầu Giấy','slug'=>'cau-giay'],
            ['name'=>'Đống Đa','slug'=>'dong-da'],
            ['name'=>'Hai Bà Trưng','slug'=>'hai-ba-trung'],
            ['name'=>'Hoàng Mai','slug'=>'hoang-mai'],
            ['name'=>'Thanh Xuân','slug'=>'thanh-xuan'],
            ['name'=>'Sóc Sơn','slug'=>'soc-son'],
            ['name'=>'Đông Anh','slug'=>'dong-anh'],
            ['name'=>'Gia Lâm','slug'=>'gia-lam'],
            ['name'=>'Nam Từ Liêm','slug'=>'nam-tu-liem'],
            ['name'=>'Thanh Trì','slug'=>'thanh-tri'],
            ['name'=>'Bắc Từ Liêm','slug'=>'bac-tu-liem'],
            ['name'=>'Mê Linh','slug'=>'me-linh'],
            ['name'=>'Hà Đông','slug'=>'ha-dong'],
            ['name'=>'Sơn Tây','slug'=>'son-tay'],
            ['name'=>'Ba Vì','slug'=>'ba-vi'],
            ['name'=>'Phúc Phọ','slug'=>'phuc-tho'],
            ['name'=>'Đan Phượng','slug'=>'dan-phuong'],
            ['name'=>'Hoài Đức','slug'=>'hoai-duc'],
            ['name'=>'Quốc Oai','slug'=>'quoc-oai'],
            ['name'=>'Thạch Thất','slug'=>'thach-that'],
            ['name'=>'Chương Mỹ','slug'=>'chuong-my'],
            ['name'=>'Thanh Oai','slug'=>'thanh-oai'],
            ['name'=>'Thường Tín','slug'=>'thuong-tin'],
            ['name'=>'Phú Xuyên','slug'=>'phu-xuyen'],
            ['name'=>'Ứng Hòa','slug'=>'ung-hoa'],
        ]);
    }
}
