@extends('admin.master')
@section('css')

@stop
@section('js')

@stop
@section('content')
    <section id="content">
        <div class="page page-create">

            <div class="pageheader">

                <h2>Tạo mới danh mục <span>// You can place subtitle here</span></h2>

                <div class="page-bar">

                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{route('admin.dashboard.index')}}"><i class="fa fa-home"></i> DDV</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.cate.index') }}">Tạo mới danh mục</a>
                        </li>
                    </ul>

                    <div class="page-toolbar">
                        <a role="button" tabindex="0" class="btn btn-lightred no-border pickDate">
                            <i class="fa fa-calendar"></i>&nbsp;&nbsp;<span></span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
                        </a>
                    </div>

                </div>

            </div>
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-8">

                        <div class="content-group border-top-lg border-top-primary">
                            <!-- Basic datatable -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h5 class="panel-title"><span class="badge badge-primary"></span> Tạo danh mục  </h5>
                                </div>

                                <div class="panel-body">
                                    @if(count($errors)>0)
                                        <div class="alert bg-danger">
                                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                            <span class="text-semibold">Lỗi!</span><br>
                                            @foreach($errors->all() as $err)
                                                {{$err}}<br>
                                            @endforeach
                                        </div>
                                    @endif
                                    <form action="{{route('admin.cate.store')}}" method="POST">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="control-label col-lg-2">Danh mục</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-git-pull-request"></i></span>
                                                        <input type="text" name="category" value="{{old('cate')}}" class="form-control" placeholder="Điền tên danh mục">
                                                    </div>
                                                </div>
                                            </div>
                                            <center>
                                                <input type="submit" style="margin-top:20px" value="Tạo mới" class="btn btn-success btn-rounded">
                                            </center>

                                        </div>
                                        <p></p>

                                    </form>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- Footer -->
    <div class="footer text-muted">
        &copy; 2019. <a href="#">Project Phòng trọ Hà Nội</a> by <a href="" target="_blank">DDV</a>
    </div>
    <!-- /footer -->
@stop
