@extends('admin.master')
@section('css')

@stop
@section('js')

@stop
@section('content')
    <section id="content">
        <div class="page page-create">

            <div class="pageheader">

                <h2>Tạo mới quản trị viên <span>// You can place subtitle here</span></h2>

                <div class="page-bar">

                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{route('admin.dashboard.index')}}"><i class="fa fa-home"></i> DDV</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.admin.index') }}">Tạo mới quản trị viên</a>
                        </li>
                    </ul>

                    <div class="page-toolbar">
                        <a role="button" tabindex="0" class="btn btn-lightred no-border pickDate">
                            <i class="fa fa-calendar"></i>&nbsp;&nbsp;<span></span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
                        </a>
                    </div>

                </div>

            </div>
            <div class="container-fluid">

                    <div class="row">
                        <div class="col-md-8">

                            <div class="content-group border-top-lg border-top-primary">
                                <!-- Basic datatable -->
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span class="badge badge-primary"></span> Tạo Tài Khoản  </h5>
                                    </div>

                                    <div class="panel-body">
                                        @if(count($errors)>0)
                                            <div class="alert bg-danger">
                                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                                <span class="text-semibold">Lỗi!</span><br>
                                                @foreach($errors->all() as $err)
                                                    {{$err}}<br>
                                                @endforeach
                                            </div>
                                        @endif
                                        <form action="{{route('admin.admin.store')}}" method="POST">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="control-label col-lg-2">Tài khoản</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-git-pull-request"></i></span>
                                                            <input type="text" name="username" value="{{old('username')}}" class="form-control" placeholder="username lớn hơn 4 kí tự">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-lg-2">Email</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-git-pull-request"></i></span>
                                                            <input type="email" value="{{old('email')}}" name="email" class="form-control" placeholder="Nhập email">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-lg-2">Họ Tên</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-git-pull-request"></i></span>
                                                            <input type="text" value="{{old('hoten')}}" name="hoten" class="form-control" placeholder="Nhập đầy đủ họ tên">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-lg-2">Mật khẩu</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-git-pull-request"></i></span>
                                                            <input type="password" value="" name="password" class="form-control password" placeholder="Mật khẩu lớn hơn 8 và nhỏ hơn 32 kí tự">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-lg-2">Nhập lại Mật khẩu</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-git-pull-request"></i></span>
                                                            <input type="password" value="" name="password_verified" class="form-control password" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Chọn quyền:</label>
                                                    <div class="col-lg-10">
                                                        <select class="form-control" name="role" >
                                                            <option value="" selected>-- Quyền --</option>
                                                            <option value="0">Quản trị viên</option>
                                                            <option value="1">Người dùng</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Trạng thái:</label>
                                                    <div class="col-lg-10">
                                                        <select class="form-control" name="status">
                                                            <option value="">-- Trạng thái --</option>
                                                            <option value="1">Kích hoạt</option>
                                                            <option  value="0">Tạm khóa</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <center>
                                                    <input type="submit" style="margin-top:20px" value="Tạo mới" class="btn btn-success btn-rounded">
                                                </center>

                                            </div>
                                            <p></p>

                                        </form>
                                    </div>


                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="content-group border-top-lg border-top-danger">
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <h5 class="panel-title"><span class="badge badge-primary"></span> Chú ý  </h5>
                                        <div class="heading-elements">
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <span class="badge badge-danger" style="margin-bottom: 5px">Mật khẩu lớn hơn 8 và nhỏ hơn 32 kí tự</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

        </div>
    </section>
    <!-- Footer -->
    <div class="footer text-muted">
        &copy; 2019. <a href="#">Project Phòng trọ Hà Nội</a> by <a href="" target="_blank">DDV</a>
    </div>
    <!-- /footer -->
@stop
