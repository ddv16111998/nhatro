@extends('admin.master')
@section('css')

@stop
@section('js')
    <script>
        $(function() {
            $('#listData').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.approved.list') !!}',
                columns: [
                    { data: 'stt', name: 'stt' },
                    { data: 'user_id', name: 'user_id' },
                    { data: 'title', name: 'title' },
                    { data: 'address', name: 'address' },
                    { data: 'created_at',name:'created_at'},
                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>
@stop
@section('content')
    <section id="content">
        <div class="page page-index">

            <div class="pageheader">

                <h2>Danh sách các phòng phê duyệt</h2>

                <div class="page-bar">

                    <ul class="page-breadcrumb">
                        <li>
                            <a href="{{route('admin.dashboard.index')}}"><i class="fa fa-home"></i> DDV</a>
                        </li>
                        <li>
                            <a href="#">Danh sách phòng trọ</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.approved.list') }}">Phê duyệt</a>
                        </li>
                    </ul>

                    <div class="page-toolbar">
                        <a role="button" tabindex="0" class="btn btn-lightred no-border pickDate">
                            <i class="fa fa-calendar"></i>&nbsp;&nbsp;<span></span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
                        </a>
                    </div>

                </div>

            </div>
            <div class="container-fluid">
                @if(session('thongbao'))
                    <div class="alert bg-success">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        <span class="text-semibold">Well done!</span>  {{session('thongbao')}}
                    </div>
                @endif
                <div class="tile-body p-0">
                    <table class="table table-bordered" id="listData">
                        <thead>
                        <tr>
                            <th>Stt</th>
                            <th>Người đăng</th>
                            <th>Tiêu đề</th>
                            <th>Địa điểm</th>
                            <th>Thời gian đăng</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>

        </div>
    </section>
@stop
