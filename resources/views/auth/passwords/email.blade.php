@extends('auth.masterAuth')
@section('content')
    <div id="wrap" class="animsition">


        <div class="page page-core page-login">

            <div class="text-center"><h3 class="text-light text-white"><span class="text-lightred">D</span>DV</h3></div>

            <div class="container w-420 p-15 bg-white mt-40 text-center">


                <h2 class="text-light text-greensea">Reset Password</h2>
                @if(session('thongbao'))
                    <div class="alert-danger">
                        <p>{{session('thongbao')}}</p>
                    </div>
                @endif
                <form class="form-validation mt-20" action="{{ route('password.email') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="email" class="form-control underline-input" placeholder="Nhập email để lấy mật khẩu mới" name="email" value="{{old('email')}}">
                    </div>
                    @error('email')
                    <span class="invalid-feedback alert-danger" role="alert">
                        <p style="font-style: italic">{{ $message }}</p>
                    </span>
                    @enderror
                    <div class="form-group text-left mt-20">
                        <button type="submit" class="btn btn-greensea b-0 br-2 mr-5">Submit</button>
                    </div>
                </form>
                <hr class="b-3x">

                <div class="social-login text-left">

                    <ul class="pull-right list-unstyled list-inline">
                        <li class="p-0">
                            <a class="btn btn-sm btn-primary b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="p-0">
                            <a class="btn btn-sm btn-info b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="p-0">
                            <a class="btn btn-sm btn-lightred b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li class="p-0">
                            <a class="btn btn-sm btn-primary b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>

                    <h5>Or login with</h5>

                </div>

            </div>

        </div>



    </div>
@stop
{{--@extends('layouts.app')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Reset Password') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    @if (session('status'))--}}
{{--                        <div class="alert alert-success" role="alert">--}}
{{--                            {{ session('status') }}--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                    <form method="POST" action="{{ route('password.email') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="form-group row">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Send Password Reset Link') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}
