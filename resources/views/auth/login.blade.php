@extends('auth.masterAuth')
@section('content')
    <div id="wrap" class="animsition">


        <div class="page page-core page-login">

            <div class="text-center"><h3 class="text-light text-white"><span class="text-lightred">D</span>DV</h3></div>

            <div class="container w-420 p-15 bg-white mt-40 text-center">


                <h2 class="text-light text-greensea">Đăng nhập</h2>
                @if(session('thongbao'))
                    <div class="alert-danger">
                        <p>{{session('thongbao')}}</p>
                    </div>
                @endif
                <form class="form-validation mt-20" action="{{route('login')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <input type="email" class="form-control underline-input" placeholder="Email" name="email">
                    </div>
                    @error('email')
                    <span class="invalid-feedback alert-danger" role="alert">
                        <p style="font-style: italic">{{ $message }}</p>
                    </span>
                    @enderror

                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control underline-input" name="password">
                    </div>
                    @error('password')
                    <span class="invalid-feedback alert-danger" role="alert">
                        <p style="font-style: italic">{{ $message }}</p>
                    </span>
                    @enderror

                    <div class="form-group text-left mt-20">
                        <button type="submit" class="btn btn-greensea b-0 br-2 mr-5">Đăng nhập</button>
                        <label class="checkbox checkbox-custom-alt checkbox-custom-sm inline-block">
                            <input type="checkbox" name="remember"><i></i> Ghi nhớ
                        </label>
                        <a href="{{route('password.request')}}" class="pull-right mt-10">Quên mật khẩu?</a>
                    </div>

                </form>

                <hr class="b-3x">

                <div class="social-login text-left">

                    <ul class="pull-right list-unstyled list-inline">
                        <li class="p-0">
                            <a class="btn btn-sm btn-primary b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="p-0">
                            <a class="btn btn-sm btn-info b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="p-0">
                            <a class="btn btn-sm btn-lightred b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li class="p-0">
                            <a class="btn btn-sm btn-primary b-0 btn-rounded-20" href="javascript:;"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>

                    <h5>Hoặc đăng nhập với</h5>

                </div>

                <div class="bg-slategray lt wrap-reset mt-40">
                    <p class="m-0">
                        <a href="" class="text-uppercase">Tạo tài khoản</a>
                    </p>
                </div>

            </div>

        </div>



    </div>
@stop
