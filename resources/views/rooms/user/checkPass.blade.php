<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from pixelgeeklab.com/html/realestast/grid-masonry-4-column.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Aug 2015 08:54:40 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Flatize - Shop HTML5 Responsive Template">
    <meta name="author" content="pixelgeeklab.com">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nhà trọ Hà Nội</title>
    {{--    css--}}
    <link rel="stylesheet" href="{{asset('rooms/css/style.css')}}">
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

    <!-- Bootstrap -->
    <link href="{{asset('rooms/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/datatables.bootstrap.min.css')}}">
    <!-- Libs CSS -->
    <link href="{{asset('rooms/css/fonts/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('rooms/vendor/owl-carousel/owl.carousel.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('rooms/vendor/owl-carousel/owl.theme.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('rooms/vendor/flexslider/flexslider.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('rooms/vendor/chosen/chosen.css')}}" media="screen">

    <!-- Theme -->
    <link href="{{asset('rooms/css/theme-animate.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme-elements.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme-blog.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme-map.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme.css')}}" rel="stylesheet">

    {{--    //css --}}
    <style type="text/css">
        .nav-top{
            border-right: 0px;
        }
        .inputPass{
            margin-bottom: 20px;
        }
    </style>
    <!-- Style Switcher-->
    <link rel="stylesheet" href="{{asset('rooms/style-switcher/css/style-switcher.css')}}">
{{--    <link href="{{asset('rooms/css/colors/red/style.html')}}" rel="stylesheet" id="layoutstyle">--}}

<!-- Theme Responsive-->
    <link href="{{asset('rooms/css/theme-responsive.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div id="page">
    <header>
        <div id="top">
            <div class="container">
                <ul class="nav nav-pills nav-top navbar-left mt-10">
                    <li class="user {{Request::is('room/user/information') ? "active" : ""}}"><a href="{{route('room.user.info')}}"><i class="fa fa-user"></i> @if(\Auth::guard('web')->check())Welcome , {{ Auth::guard('web')->user()->name}}@endif</a></li>
                    <li class="index {{Request::is('room/index') ? "active" : ""}}"><a href="{{route('room.index')}}">Trang chủ</a></li>
                    @if(!\Auth::guard('web')->check())
                        <li class="login"><a href="{{route('room.login')}}">Đăng nhập</a></li>
                        <li class="registration"><a href="{{route('room.create')}}">Đăng ký</a></li>
                    @endif
                    <li class="post {{Request::is('room/post') ? "active" : ""}}"><a href="{{route('room.post')}}">Đăng tin</a></li>
                    @if(\Auth::guard('web')->check())
                        <li class="logout"><a href="{{route('room.logout')}}">Đăng xuất</a></li>
                    @endif
                </ul>
                <ul class="nav nav-pills nav-top navbar-right">
                    <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Email"><i class="fa fa-envelope-o"></i></a></li>
                    <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
    </header>
    <div role="main" class="main pgl-bg-grey">
        <section class="pgl-properties pgl-bg-grey">
            <div class="container">
                <form action="{{route('room.user.checkPass')}}" method="POST">
                    @csrf
                    <div class="row inputPass" id="inputPass">
                        <div class="form-group">
                            <div class="col-md-3"><label for="">Nhập mật khẩu cũ</label></div>
                            <div class="col-md-6"><input type="text" class="form-control" name="oldPass" id="oldPass"></div>
                            <div class="col-md-3"><button class="btn btn-primary" type="submit">Tiếp tục</button></div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('rooms/vendor/jquery.min.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('rooms/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('rooms/vendor/owl-carousel/owl.carousel.js')}}"></script>
<script src="{{asset('rooms/vendor/flexslider/jquery.flexslider-min.js')}}"></script>
<script src="{{asset('rooms/vendor/chosen/chosen.jquery.min.js')}}"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>
<script src="{{asset('rooms/vendor/gmap/gmap3.infobox.min.js')}}"></script>
<script src="{{asset('rooms/vendor/masonry/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('rooms/vendor/masonry/masonry.pkgd.min.js')}}"></script>

<script src="{{asset('assets/js/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables/extensions/dataTables.bootstrap.js')}}"></script>
<!-- Theme Initializer -->
<script src="{{asset('rooms/js/theme.plugins.js')}}"></script>
<script src="{{asset('rooms/js/theme.js')}}"></script>

<!-- Style Switcher -->
<script type="text/javascript" src="{{asset('rooms/style-switcher/js/switcher.js')}}"></script>
</body>

<!-- Mirrored from pixelgeeklab.com/html/realestast/grid-masonry-4-column.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Aug 2015 08:54:40 GMT -->
</html>

