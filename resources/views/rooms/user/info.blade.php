<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from pixelgeeklab.com/html/realestast/grid-masonry-4-column.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Aug 2015 08:54:40 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Flatize - Shop HTML5 Responsive Template">
    <meta name="author" content="pixelgeeklab.com">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nhà trọ Hà Nội</title>
    {{--    css--}}
    <link rel="stylesheet" href="{{asset('rooms/css/style.css')}}">
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

    <!-- Bootstrap -->
    <link href="{{asset('rooms/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/vendor/datatables/datatables.bootstrap.min.css')}}">
    <!-- Libs CSS -->
    <link href="{{asset('rooms/css/fonts/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('rooms/vendor/owl-carousel/owl.carousel.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('rooms/vendor/owl-carousel/owl.theme.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('rooms/vendor/flexslider/flexslider.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('rooms/vendor/chosen/chosen.css')}}" media="screen">

    <!-- Theme -->
    <link href="{{asset('rooms/css/theme-animate.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme-elements.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme-blog.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme-map.css')}}" rel="stylesheet">
    <link href="{{asset('rooms/css/theme.css')}}" rel="stylesheet">

{{--    //css --}}
    <style type="text/css">
        .nav-top{
            border-right: 0px;
        }
        #avatar{
            position: relative;
        }
        #changeFile{
            position: absolute;
            bottom: 20px;
            left: 0;
            width: 100%;
            height: 30px;
            opacity: 0;
        }
        #avatar:hover #changeFile{
            opacity: 1;
        }
    </style>
    <!-- Style Switcher-->
    <link rel="stylesheet" href="{{asset('rooms/style-switcher/css/style-switcher.css')}}">
{{--    <link href="{{asset('rooms/css/colors/red/style.html')}}" rel="stylesheet" id="layoutstyle">--}}

<!-- Theme Responsive-->
    <link href="{{asset('rooms/css/theme-responsive.css')}}" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div id="page">
    <header>
        <div id="top">
            <div class="container">
                <ul class="nav nav-pills nav-top navbar-left mt-10">
                    <li class="user {{Request::is('room/user/information') ? "active" : ""}}"><a href="{{route('room.user.info')}}"><i class="fa fa-user"></i> @if(\Auth::guard('web')->check())Welcome , {{ Auth::guard('web')->user()->name}}@endif</a></li>
                    <li class="index {{Request::is('room/index') ? "active" : ""}}"><a href="{{route('room.index')}}">Trang chủ</a></li>
                    @if(!\Auth::guard('web')->check())
                        <li class="login"><a href="{{route('room.login')}}">Đăng nhập</a></li>
                        <li class="registration"><a href="{{route('room.create')}}">Đăng ký</a></li>
                    @endif
                    <li class="post {{Request::is('room/post') ? "active" : ""}}"><a href="{{route('room.post')}}">Đăng tin</a></li>
                    @if(\Auth::guard('web')->check())
                        <li class="logout"><a href="{{route('room.logout')}}">Đăng xuất</a></li>
                    @endif
                </ul>
                <ul class="nav nav-pills nav-top navbar-right">
                    <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Email"><i class="fa fa-envelope-o"></i></a></li>
                    <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
    </header>
    <div role="main" class="main pgl-bg-grey">
        <section class="pgl-properties pgl-bg-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="image" style="text-align: center">
                            <img style="border-radius: 50%" src="{{(\Auth::guard('web')->user()->avatar)  ? (asset('rooms/images/user').'/'.\Auth::guard('web')->user()->avatar) : (asset('rooms/images/user/no-avatar.jpg'))}}" alt="avatar" height="300px" width="300px"><br><br>
                            Account : <b>{{\Auth::guard('web')->user()->name}}</b>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li><b>Tham gia vào : </b>{{\Auth::guard('web')->user()->created_at}}</li>
                            <li><b>Trạng thái tài khoản : </b>{{\Auth::guard('web')->user()->status == 1 ? 'Hoạt động' : 'Tạm khóa'}}</li>
                            <li><b>Quyền tài khoản : </b>{{\Auth::guard('web')->user()->role == 1 ? 'Người dùng' : 'Quản trị viên'}}</li>
                            <li><b>Email : </b>{{\Auth::guard('web')->user()->email}}</li>
                            <li><b>Biệt danh : </b>{{\Auth::guard('web')->user()->username}}</li>
                        </ul>
                        <div class="change-user">
                            <a href="" class="btn btn-lg btn-warning" data-toggle="modal" data-target="#modalEdit" data-placement="top" title="Sửa tài khoản" style="border-radius: 50%"><span class="fa fa-edit"></span></a>
                            <!-- Modal Sửa -->
                            <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Sửa tài khoản</h5>
                                            <a href="{{route('room.user.checkPass')}}" class="btn btn-primary" style="float: right">Đổi mật khẩu</a>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('room.user.update',\Auth::guard('web')->user()->id)}}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="">Ảnh đại diện</label>
                                                    <center>
                                                        <div id="avatar">
                                                            <img id="img" src="{{asset('rooms/images/user').'/'.\Auth::guard('web')->user()->avatar}}" alt="" width="200px" height="200px">
                                                            <div id="changeFile">
                                                                <a href="#" class="btn btn-info btn-lg">
                                                                    <span class="glyphicon glyphicon-camera"></span> Cập nhật
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </center>
                                                    <input type="file" class="hidden" value="Chọn file" id="avatarUser" name="avatar">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Họ tên</label>
                                                    <input type="text" class="form-control" id="" name="name" value="{{\Auth::guard('web')->user()->name}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Biệt danh</label>
                                                    <input type="text" class="form-control" id="" name="username" value="{{\Auth::guard('web')->user()->username}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Email</label>
                                                    <input type="email" class="form-control" id="" name="email" value="{{\Auth::guard('web')->user()->email}}" required>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Trạng thái tài khoản</label>
                                                    <select name="status" id="status" class="form-control">
                                                        <option value="1">Không thay đổi</option>
                                                        <option value="0">Khóa tài khoản</option>
                                                    </select>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                                                    <button type="submit" class="btn btn-primary">Lưu</button>
                                                </div>
                                            </form>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-lg btn-danger" data-toggle="modal" data-target="#modalDelete" data-placement="top" title="Xóa tài khoản" style="border-radius: 50%"><span class="fa fa-trash"></span></button>
                            <!-- Modal Xóa -->
                            <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
                                        </div>
                                        <div class="modal-body">
                                            Bạn có chắc chắn muốn xóa tài khoản không?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                                            <a href="{{route('room.user.destroy',\Auth::guard('web')->user()->id)}}" class="btn btn-primary">Xóa</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="pgl-advanced-post pgl-bg-light" style="padding-top: 100px">
        <div class="container">
            <div class="row">
                <h3><b>Tin đăng phòng</b></h3>
                @if(session('thongbao'))
                    <div class="alert bg-success">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        <span class="text-semibold">Well done!</span>  {{session('thongbao')}}
                    </div>
                @endif
                @if(\Auth::guard('web')->user()->room()->get()->count() == 0)
                    <div class="col-md-12">
                        <div class="alert alert-danger" role="alert">
                            Bạn chưa có tin đăng phòng cho thuê , thử đăng ngay.
                        </div>
                    </div>
                @else
                        <div class="col-md-12">
                            <table class="table table-bordered" id="listPost">
                                <thead>
                                <tr>
                                    <th>Stt</th>
                                    <th>Tiêu đề</th>
                                    <th>Danh mục</th>
                                    <th>Giá phòng</th>
                                    <th>Lượt xem</th>
                                    <th>Tình trạng tin</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                @endif
            </div>
        </div>
    </section>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('rooms/vendor/jquery.min.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('rooms/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('rooms/vendor/owl-carousel/owl.carousel.js')}}"></script>
<script src="{{asset('rooms/vendor/flexslider/jquery.flexslider-min.js')}}"></script>
<script src="{{asset('rooms/vendor/chosen/chosen.jquery.min.js')}}"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>
<script src="{{asset('rooms/vendor/gmap/gmap3.infobox.min.js')}}"></script>
<script src="{{asset('rooms/vendor/masonry/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('rooms/vendor/masonry/masonry.pkgd.min.js')}}"></script>

<script src="{{asset('assets/js/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/datatables/extensions/dataTables.bootstrap.js')}}"></script>
<!-- Theme Initializer -->
<script src="{{asset('rooms/js/theme.plugins.js')}}"></script>
<script src="{{asset('rooms/js/theme.js')}}"></script>

<!-- Style Switcher -->
<script type="text/javascript" src="{{asset('rooms/style-switcher/js/switcher.js')}}"></script>
<script>
    $(function() {
        $('#listPost').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('room.user.info') !!}',
            columns: [
                { data: 'stt', name: 'stt' },
                { data: 'title', name: 'title' },
                { data: 'cate_id', name: 'cate_id' },
                { data: 'price', name: 'price' },
                { data: 'count_views', name: 'count_views' },
                { data : 'approve', name:'approve'},
                { data: 'action', name: 'action' }
            ]
        });
        $('#changeFile').click(function () {
            $('#avatarUser').click();
            // $.ajax({
            //     type:"POST",
            //
            // })
        })
    });
</script>
</body>

<!-- Mirrored from pixelgeeklab.com/html/realestast/grid-masonry-4-column.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 16 Aug 2015 08:54:40 GMT -->
</html>

