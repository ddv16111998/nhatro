@extends('rooms.master')
@section('css')
    <style type="text/css">
        .content .pgl-featured, .content .pgl-properties {
             padding-top: 70px;
         }
        /*#listComments{*/
        /*    padding-top: 40px;*/
        /*}*/
        /*#avatar{*/
        /*    width: 30px;*/
        /*    height: 30px;*/
        /*    border-radius: 50%;*/
        /*}*/
        #map {
            height: 100%;
        }
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
@stop

@section('js')
{{--    <script>--}}
{{--        function loadComment() {--}}
{{--            $.ajaxSetup({--}}
{{--                headers: {--}}
{{--                    'X-CSRF-TOKEN': '{{ csrf_token() }}',--}}
{{--                }--}}
{{--            });--}}
{{--            var formData = new FormData();--}}
{{--            formData.append('id_post')--}}
{{--            $.ajax({--}}
{{--                url:"{{route('room.store')}}",--}}
{{--                dataType:"html",--}}
{{--                data: formData--}}
{{--            })--}}
{{--                .done(function (data) {--}}
{{--                    $('#commune_id').html(data);--}}
{{--                })--}}
{{--        }--}}
{{--    </script>--}}
@stop
@section('content')
    <div id="map"></div>
    <!-- Begin Main -->
    <div role="main" class="main pgl-bg-grey">
        <!-- Begin content with sidebar -->
        <div class="container">
            <div class="row">
                <div class="col-md-9 content">
                    <section class="pgl-pro-detail pgl-bg-light">
                        <div id="slider" class="flexslider">
                            <ul class="slides" >
                                @foreach(json_decode($room->images) as $value)
                                    <li>
                                        <img src="{{ URL::to('/rooms/images/rooms') }}/{{ $value }}" alt="">
                                        <span class="property-thumb-info-label">
											<span class="label price">{{number_format($room->price)}}</span>
											<span class="label forrent">Rent</span>
										</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div id="carousel" class="flexslider">
                            <ul class="slides">
                                @foreach(json_decode($room->images) as $value)
                                    <li> <img src="{{ URL::to('/rooms/images/rooms') }}/{{ $value }}" alt=""></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="pgl-detail">
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul class="list-unstyled amenities amenities-detail">
                                        <li><strong>Kiểu nhà:</strong> {{$room->category->name}}</li>
                                        <li><strong>Diện tích:</strong> {{$room->acreage}}<sup>m2</sup></li>
                                        <li><address><i class="icons icon-location"></i> {{$room->address}}</address></li>
                                        <li><i class="fa fa-eye"></i> {{$room->count_views}}</li>
                                    </ul>
                                </div>
                                <div class="col-sm-8">
                                    <h2>{{$room->title}}</h2>
                                    <p>{{ (strip_tags($room->description)) }}</p>
                                </div>
                            </div>

                            <div class="tab-detail">
                                <h3>Thông tin</h3>
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default pgl-panel">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Tiện ích</a> </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <ul>
                                                    <li><strong>AC:</strong> Ceiling Fan(s), Central</li>
                                                    <li><strong>Acres Source:</strong> Assessor</li>
                                                    <li><strong>Bathrooms Description:</strong> Stall Shower</li>
                                                    <li><strong>Bathrooms Features:</strong> Main Floor Master Bedroom</li>
                                                    <li><strong>Dining Area:</strong> Family Kitchen</li>
                                                    <li><strong>Lot Description:</strong> Curbs-Walks</li>
                                                    <li><strong>Mot Dimensions:</strong> 70x100</li>
                                                    <li><strong>Lot Size Source:</strong> Assessor</li>
                                                    <li><strong>Parking Type:</strong> Direct Garage Access, Driveway, Garage Door Opener</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default pgl-panel">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">Video</a> </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body"> <p>Không tải lên video</p> </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default pgl-panel">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">Map</a> </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p>Đường dẫn chỉ đường từ vị trí của bạn đến địa điểm phòng đang xem!!</p>
                                                <div id="contact-map" class="pgl-bg-light"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default pgl-panel">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseFouth" class="collapsed">Liên hệ</a> </h4>
                                        </div>
                                        <div id="collapseFouth" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="pgl-agent-item pgl-bg-light">
                                                    <div class="row pgl-midnarrow-row">
                                                        <div class="col-xs-4">
                                                            <div class="img-thumbnail-medium">
                                                                <a href="agentprofile.html"><img alt="" class="img-responsive" src=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-8">
                                                            <div class="pgl-agent-info">
                                                                <small>NO.1</small>
                                                                <h4><a href="agentprofile.html">John Smith</a></h4>
                                                                <address>
                                                                    <i class="fa fa-phone"></i> Số điện thoại : {{$room->phone}}<br>
                                                                    <i class="fa fa-envelope-o"></i> Mail: <a href="">{{$room->getUser->email}}</a>
                                                                </address>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    @if(\Auth::guard('web')->check())
                        <section class="comment">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="fb-comments" data-href="http://nhatro.com/room/detailsRoom/{{$room->id}}" data-width="100%" data-numposts="8"></div>
{{--                                        <h2>Mời bạn đánh giá hoặc đặt câu hỏi</h2>--}}
{{--                                        <form action="{{route('room.comment',$room->id)}}" method="POST" enctype="multipart/form-data">--}}
{{--                                            @csrf--}}
{{--                                            <textarea class="form-control" id="editor1" name="comment"></textarea>--}}
{{--                                            <button class="btn btn-primary mt-4" id="btnComment">Gửi</button>--}}
{{--                                        </form>--}}
                                    </div>
{{--                                    <div class="col-md-12" id="listComments">--}}
{{--                                        @foreach($comments as $comment)--}}
{{--                                            <div class="alert alert-danger">--}}
{{--                                                <img src="{{( $comment->userComment->avatar) ? (asset('rooms/images/user').'/'.$comment->userComment->avatar) : (asset('rooms/images/user/no-avatar.jpg'))}}" alt="" id="avatar"><span style="color: black"><strong> {{$comment->userComment->name}}</strong> : {{strip_tags($comment->comment)}}</span>--}}
{{--                                            </div>--}}
{{--                                        @endforeach--}}
{{--                                    </div>--}}
{{--                                    {{ $comments->links() }}--}}
                                </div>
                        </section>
                    @endif
                    <!-- Begin Related properties -->
                    <section class="pgl-properties mt-5">
                        <h2>Các phòng liên quan</h2>
                        <div class="row">
                            <div class="owl-carousel pgl-pro-slide" data-plugin-options='{"items": 3, "itemsDesktop": 3, "singleItem": false, "autoPlay": false, "pagination": false}'>
                                @foreach($roomsRandom as $value)
                                    <div class="col-md-12 animation">
                                    <div class="pgl-property">
                                        <div class="property-thumb-info">
                                            <div class="property-thumb-info-image">
                                                <img alt="" class="" src="{{ URL::to('/rooms/images/rooms') }}/{{ json_decode($value->images)[0] }}" height="150px" width="100%">
                                                <span class="property-thumb-info-label">
														<span class="label price">{{$value->price}}</span>
														<span class="label forrent">Rent</span>
													</span>
                                            </div>
                                            <div class="property-thumb-info-content">
                                                <h3><a href="{{route('room.details',$value->id)}}"><b>{{$value->title}}</b></a></h3>
                                                <address><b>Địa điểm:</b> {{$value->address}}</address>
                                            </div>
                                            <div class="amenities clearfix">
                                                <ul class="pull-left">
                                                    <li><strong>Diện tích:</strong> {{$value->acreage}}<sup>m2</sup></li>
                                                </ul>
                                                <ul class="pull-right">
                                                    <li><i class="fa fa-eye"></i> {{$value->count_views}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </section>
                    <!-- End Related properties -->

                </div>
                <div class="col-md-3 sidebar">
                </div>
            </div>
        </div>
        <!-- End content with sidebar -->

    </div>
    <!-- End Main -->
@stop
