<header>
    <div id="top">
        <div class="container">
            <ul class="nav nav-pills nav-top navbar-left mt-10">
                <li class="user {{Request::is('room/user/information') ? "active" : ""}}"><a href="{{route('room.user.info')}}"><i class="fa fa-user"></i> @if(\Auth::guard('web')->check())Welcome , {{ Auth::guard('web')->user()->name}}@endif</a></li>
                <li class="index {{Request::is('room/index') ? "active" : ""}}"><a href="{{route('room.index')}}">Trang chủ</a></li>
                @if(!\Auth::guard('web')->check())
                    <li class="login"><a href="{{route('room.login')}}">Đăng nhập</a></li>
                    <li class="registration"><a href="{{route('room.create')}}">Đăng ký</a></li>
                @endif
                <li class="post {{Request::is('room/post/index') ? "active" : ""}}"><a href="{{route('room.post')}}">Đăng tin</a></li>
                @if(\Auth::guard('web')->check())
                    <li class="logout"><a href="{{route('room.logout')}}">Đăng xuất</a></li>
                @endif
            </ul>
            <ul class="nav nav-pills nav-top navbar-right">
                <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Email"><i class="fa fa-envelope-o"></i></a></li>
                <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" title="" data-placement="bottom" data-toggle="tooltip" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
            </ul>
        </div>
    </div>
    <nav class="navbar navbar-default pgl-navbar-main" role="navigation">
        <div class="container">
            <div class="navbar-header">

            <div class="navbar-collapse collapse width">
                <ul class="nav navbar-nav pull-right">
                    @foreach($cates as $value)
                        <li class=""><a href="{{route('room.byCate',['cateId'=>$value->id])}}">{{$value->name}}</a>
                        </li>
                    @endforeach


                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
</header>
<!-- Begin page top -->
<section class="page-top">
        @if(Request::is('room/index'))
            <!-- Begin Main Slide -->
                <section class="main-slide">
                    <div id="owl-main-slide" class="owl-carousel pgl-main-slide" data-plugin-options='{"autoPlay": true}'>
                        <div class="item" id="item1"><img src="{{asset('rooms/images/slides/slider1.jpg')}}" alt="Photo" class="img-responsive">
                            <div class="item-caption">
                                <div class="container">
                                    <div class="property-info">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item" id="item2"><img src="{{asset('rooms/images/slides/slider2.jpg')}}" alt="Photo" class="img-responsive">
                            <div class="item-caption">
                                <div class="container">
                                    <div class="property-info">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item" id="item3"><img src="{{asset('rooms/images/slides/slider3.jpg')}}" alt="Photo" class="img-responsive">
                            <div class="item-caption">
                                <div class="container">
                                    <div class="property-info">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End Main Slide -->
        @else
                <div class="container">
                    <div class="page-top-in">
                    </div>
                </div>
        @endif

</section>
<!-- End page top -->

<!-- Begin Advanced Search -->
<section class="pgl-advanced-search pgl-bg-light">
    <div class="container">
        <form name="advancedsearch" method="GET" action="">
            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
{{--                        <label class="sr-only" for="">Tỉnh / TP</label>--}}
                        <select id="header_district_id" name="header_district_id" class="form-control" onchange="get_location($(this))">
                            <option selected="selected" value=""> -- Chọn tỉnh / TP --</option>
                            @foreach($districts as $district)
                                <option value="{{$district->id}}">{{$district->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
{{--                        <label class="sr-only" for="commune_id">Xã / Phường</label>--}}
                        <select id="header_commune_id" name="header_commune_id" class="form-control">
                            <option selected="selected" value=""> -- Chọn quận / huyện -- </option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label class="sr-only" for="cate_id">Kiểu nhà</label>
                        <select id="cate_id" name="cate_id" class="chosen-select">
                            <option selected="selected" value=""> -- Chọn kiểu nhà -- </option>
                            @foreach($cates as $cate)
                                <option value="{{$cate->id}}">{{$cate->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <div class="row pgl-narrow-row">
                            <div class="col-xs-6">
                                <label class="sr-only" for="search-minprice">Price From</label>
                                <select id="minprice" name="search-minprice" data-placeholder="Price From" class="chosen-select">
                                    <option selected="selected" value="Price From">Giá tối thiểu</option>
                                    <option value="500000">500.000</option>
                                    <option value="1000000">1.000.000</option>
                                    <option value="1500000">1.500.000</option>
                                    <option value="2000000">2.000.000</option>
                                    <option value="2500000">2.500.000</option>
                                    <option value="3000000">3.000.000</option>
                                    <option value="4000000">4.000.000</option>
                                    <option value="5000000">5.000.000</option>
                                    <option value="7000000">7.000.000</option>
                                </select>
                            </div>
                            <div class="col-xs-6">
                                <label class="sr-only" for="search-maxprice">Price To</label>
                                <select id="maxprice" name="search-maxprice" data-placeholder="Price To" class="chosen-select">
                                    <option selected="selected" value="Price To">Giá tối đa</option>
                                    <option value="8000000">8.000.000</option>
                                    <option value="9000000">9.000.000</option>
                                    <option value="10000000">10.000.000</option>
                                    <option value="11000000">11.000.000</option>
                                    <option value="12000000">12.000.000</option>
                                    <option value="13000000">13.000.000</option>
                                    <option value="14000000">14.000.000</option>
                                    <option value="15000000">15.000.000</option>
                                    <option value="17000000">17.000.000</option>
                                    <option value="20000000">20.000.000</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <button type="button" id="btn-search" class="btn btn-block btn-primary" style="height:auto!important;">Tìm kiếm <i class="iconload fa fa-spinner fa-pulse" style="display:none"></i></button>
                    </div>
                </div>
            </div>

        </form>
    </div>
    <div class="load-ajax" id="load-ajax">
        <div class="container">
            <div class="row">
                <div class="result">
                    @if(session('thongbao'))
                        <div class="alert bg-danger">
                            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                            <span class="text-semibold"></span>  {{session('thongbao')}}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Advanced Search -->
